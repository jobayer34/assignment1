<?php
namespace Assignment;
require_once("vendor/autoload.php");

use Assignment\Services\Task1;
use Assignment\Services\Task2;
use Assignment\Services\Task3;

$task1 = new Task1(1,20, ' ');
$task1->prepareArray();

echo 'Task v1:'.PHP_EOL;
echo $task1->getFullPattern().PHP_EOL;

$task2 = new Task2(1, 15, '-');
$task2->prepareArray();

echo 'Task v2:'.PHP_EOL;
echo $task2->getFullPattern().PHP_EOL;

$task3 = new Task3(1, 10, '-');
$task3->prepareArray();

echo 'Task v3:'.PHP_EOL;
echo $task3->getFullPattern().PHP_EOL;


