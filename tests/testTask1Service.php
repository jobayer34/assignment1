<?php

use Assignment\Services\Task1;
use Assignment\Services\Task2;
use PHPUnit\Framework\TestCase;

class testTask1Service extends TestCase
{
    public function dataProviderForPattern2(){
        return [
           [1,20,'',3],
            [1,20,'',6],
            [1,20,'',9]
        ];
    }

    public function dataProviderForPattern3(){
        return [
            [1,20,'',5],
            [1,20,'',10]
        ];
    }

    /**
     * Testing first pattern
     */
    public function testFindIndividualNumberPattern1()
    {
        $expecetd = 'papow';
        $task1 = new Task1(1,20, '');
        $actual = $task1->findIndividualNumberPattern(15);

        $this->assertSame($expecetd, $actual,"$actual found");
    }

    /**
     * Testing second pattern
     * @dataProvider dataProviderForPattern2
     */
    public function testFindIndividualNumberPattern2($startNumber, $endNumber, $separator, $number)
    {
        $expecetd = 'pa';
        $task1 = new Task1($startNumber,$endNumber, $separator);
        $actual = $task1->findIndividualNumberPattern($number);

        $this->assertSame($expecetd, $actual,"$actual found");
    }

    /**
     * Testing second pattern
     * @dataProvider dataProviderForPattern3
     */
    public function testFindIndividualNumberPattern3($startNumber, $endNumber, $separator, $number)
    {
        $expecetd = 'pow';
        $task1 = new Task1($startNumber,$endNumber, $separator);
        $actual = $task1->findIndividualNumberPattern($number);

        $this->assertSame($expecetd, $actual,"$actual found");
    }

    /**
     * Test Full class execution
     */
    public function testTask1Class(){
        $expected = '1 2 pa 4 pow pa 7 8 pa pow 11 pa 13 14 papow 16 17 pa 19 pow';
        $task1 = new Task1(1,20,' ');
        $task1->prepareArray();
        $actual = $task1->getFullPattern();

        $this->assertSame($expected, $actual);
    }
}