<?php
namespace Assignment\Services;

class Task3 implements TaskInterface
{
    private $startNumber;
    private $endNumber;
    private $resultArray = [];
    private $separator;

    public function __construct($startNumber, $endNumber, $separator)
    {
        $this->startNumber = $startNumber;
        $this->endNumber = $endNumber;
        $this->separator = $separator;
    }

    public function prepareArray()
    {
        for($i = $this->startNumber; $i <= $this->endNumber; $i++){
            array_push($this->resultArray,$this->findIndividualNumberPattern($i)) ;
        }
    }

    public function getFullPattern(){
        return implode($this->separator, $this->resultArray);
    }

    public function findIndividualNumberPattern($number){
        $result = $number;

        if(($number == 1 || $number == 4 || $number == 9) && $number > 5)  $result = 'jofftchoff';
        else if($number == 1 || $number == 4 || $number == 9) $result = 'joff';
        else if($number > 5) $result = 'tchoff';

        return $result;
    }

}