<?php
namespace Assignment\Services;

class Task1 implements TaskInterface
{
    private $startNumber;
    private $endNumber;
    private $resultArray = [];
    private $separator;

    public function __construct($startNumber, $endNumber, $separator)
    {
        $this->startNumber = $startNumber;
        $this->endNumber = $endNumber;
        $this->separator = $separator;
    }

    public function prepareArray(){
        for($i = $this->startNumber; $i <= $this->endNumber; $i++){
            array_push($this->resultArray,$this->findIndividualNumberPattern($i)) ;
        }
    }

    /**
     * @return string
     */
    public function getFullPattern(){
        return implode($this->separator, $this->resultArray);
    }

    /**
     * @param $number
     * @return mixed|string
     */
    public function findIndividualNumberPattern($number){
        $result = $number;

        if($number%3 == 0 && $number%5 == 0) $result = 'papow';
        else if($number%3 == 0) $result = 'pa';
        else if($number%5 == 0) $result = 'pow';

        return $result;
    }

}