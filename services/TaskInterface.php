<?php
namespace Assignment\Services;

interface TaskInterface
{
    public function prepareArray();
    public function getFullPattern();
    public function findIndividualNumberPattern($number);
}