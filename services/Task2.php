<?php
namespace Assignment\Services;

class Task2 implements TaskInterface
{

    private $startNumber;
    private $endNumber;
    private $resultArray = [];
    private $separator;

    public function __construct($startNumber, $endNumber, $separator)
    {
        $this->startNumber = $startNumber;
        $this->endNumber = $endNumber;
        $this->separator = $separator;
    }

    public function prepareArray()
    {
        for($i = $this->startNumber; $i <= $this->endNumber; $i++){
            array_push($this->resultArray,$this->findIndividualNumberPattern($i)) ;
        }

    }

    public function getFullPattern(){
        return implode($this->separator, $this->resultArray);
    }

    public function findIndividualNumberPattern($number){
        $result = $number;

        if($number%2==0 && $number%7 == 0) $result = 'hateeho';
        else if($number%2 == 0) $result = 'hatee';
        else if($number%7 == 0) $result = 'ho';

        return $result;
    }

}